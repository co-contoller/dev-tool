package com.example.demo.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.nio.file.FileSystems
import java.nio.file.Paths

@Configuration
internal class AppConfig {

    //@Value("\${workdir.path}")
    val workDirPath: String = "C:\\dev\\co-controller\\esp-module\\firmware"

    @Bean
    fun watchService() = FileSystems.getDefault()
            .newWatchService()

    @Bean
    fun workspacePath() = Paths.get(workDirPath)

}