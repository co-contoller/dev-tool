package com.example.demo.data

internal data class UpdateInfoData(
        val files: List<String>
)