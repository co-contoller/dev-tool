package com.example.demo.controller

import com.example.demo.AppService
import com.example.demo.AppService.Companion.log
import com.example.demo.data.UpdateInfoData
import lombok.extern.slf4j.Slf4j
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.File
import javax.servlet.http.HttpServletResponse

@Slf4j
@RestController
@RequestMapping("update")
internal class AppController(
        private val appService: AppService
) {

    @GetMapping("info")
    fun getUpdateInfo(
            @RequestParam(required = false) inc: String? = null
    ): ResponseEntity<UpdateInfoData> {
        log.info("Update REQUEST!!!!")
//        if (inc == null) { appService.reloadFilesList() }
        val filesList = appService.getWorkspaceFilesList()
        return ResponseEntity.ok(UpdateInfoData(filesList))
    }

    @GetMapping("file/{fileName}")
    fun getFileData(@PathVariable fileName: String, responseServlet: HttpServletResponse): ResponseEntity<ByteArray> {
        val fileBytes = File(appService.workspacePath.toFile(), fileName)
                .readBytes()
        return ResponseEntity.ok().body(fileBytes)
    }

}