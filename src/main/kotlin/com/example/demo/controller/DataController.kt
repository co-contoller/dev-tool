package com.example.demo.controller

import com.example.demo.AppService
import lombok.extern.slf4j.Slf4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.concurrent.atomic.AtomicInteger

@RestController
@RequestMapping("/controller")
internal class DataController {

    companion object {
        val log: Logger = LoggerFactory.getLogger(DataController::class.java)
    }

    val atomicInt = AtomicInteger(0)
    val tasksList = arrayListOf<String>()

    @PostMapping("/update")
    fun postControllerData(@RequestBody requestData: ControllerDataReq): ResponseEntity<ControllerDataRes> {
        log.info("controller update!")
        log.info(requestData.toString())

        val response = ControllerDataRes("OK")

//        if (atomicInt.incrementAndGet() > 5) {
//            log.info("UPDATE")
            //response.tasks.add("UPDATE")
//            atomicInt.set(0)
//        }

        if (tasksList.size > 0)
            log.info(tasksList.toString())

        response.tasks.addAll(tasksList)
        tasksList.clear()

        return ResponseEntity.ok(response)
    }

    @GetMapping("update")
    fun updateTask() {
        tasksList.add("UPDATE")
    }

    @GetMapping("reboot")
    fun rebootTask() {
        tasksList.add("REBOOT")
    }

}

internal data class ControllerDataRes(
        val status: String,
        var tasks: ArrayList<String> = arrayListOf<String>()
)

internal data class ControllerDataReq(
        val temp: Int,
        val alarm: AlarmData,
        var config: ConfigData? = null
)

internal data class AlarmData(
        val triggered: Boolean,
        val name: String,
        val muted: Boolean,
        val mute_time: Int
)

internal data class ConfigData(
        val warn_alarm: AlarmConfigData,
        val danger_alarm: AlarmConfigData,
        val alarm_mute_time: Int
)

internal data class AlarmConfigData(
        val trigger: Int,
        val noise_time: Int,
        val break_time: Int
)