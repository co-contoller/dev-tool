package com.example.demo

import lombok.extern.slf4j.Slf4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.nio.file.Path
import java.nio.file.StandardWatchEventKinds.*
import java.nio.file.WatchService
import javax.annotation.PostConstruct

@Slf4j
@Service
internal class AppService(
        val workspacePath: Path,
        private val watchService: WatchService
) {

    companion object {

        val log: Logger = LoggerFactory.getLogger(AppService::class.java)

        val WORKSPACE_TARGET_EVENTS_LIST = arrayOf(ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY)
        val WORKSPACE_IGNORED_FILES_LIST = arrayOf("boot.py", "boot.json", ".git")

    }

    private var workspaceFilesList: MutableList<String> = arrayListOf()

    @PostConstruct
    fun initDirWatcher() {
        workspacePath.register(watchService, WORKSPACE_TARGET_EVENTS_LIST)
        reloadFilesList()
    }

    @Scheduled(fixedRate = 2000)
    fun updateWorkspaceChanges() {
        watchService.poll()?.let { watchKey ->
            watchKey.pollEvents().forEach { event ->
                when (event.kind()) {
                    ENTRY_CREATE -> addFile(event.context().toString())
                    ENTRY_MODIFY -> addFile(event.context().toString())
                    ENTRY_DELETE -> removeFile(event.context().toString())
                }
            }
            watchKey.reset()
        }
    }

    fun reloadFilesList() {
        workspaceFilesList = workspacePath.toFile().listFiles()
                .filter { file -> file != null && file.isFile &&
                            file.name !in WORKSPACE_IGNORED_FILES_LIST
                }.map { file -> file.name }
                .toMutableList()
//        log.info(workspaceFilesList.toString())
//        log.info("Files list reloaded!")
    }

    fun addFile(name: String) {
        if (name in workspaceFilesList) {
            log.info("File /${name}/ already exits/modified!")
            return
        }
        workspaceFilesList.add(name)
//        log.info("File /${name}/ added!")
    }

    fun removeFile(name: String) {
        if (name !in workspaceFilesList) {
//            log.info("File /${name}/ not found!")
            return
        }
        workspaceFilesList.remove(name)
//        log.info("File /${name}/ removed!")
    }

    fun getWorkspaceFilesList(): MutableList<String> {
        val updateFiles = arrayListOf<String>()
        updateFiles.addAll(workspaceFilesList)
        //workspaceFilesList.clear()
        return updateFiles
    }

}